package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBookingService;

/**
 * TODO: Implement the SeatBookingEngine according to the instructions in the Readme.
 * 
 * Model classes already exist for Seat, SeatHold, and ReservationConfirmation. Their definition and javadoc are included in the classpath jar.
 * 
 * All seat interactions should be achievable via the parent class's included SeatDao reference. It can be accessed within SeatBookingEngine via:
 * 
 * <pre>
 * this.getDao()
 * </pre>
 * 
 * The public methods on SeatDao include the following list.
 * 
 * <pre>
 * // Returns all the seats 
 * public Seat[][] getSeats();
 * 
 * // Returns the number of rows in the venue
 * public int getNumberOfRows();
 * 
 * // Returns the number of columns in the venue
 * public int getNumberOfColumns();
 * 
 * // Get all the Seats being held, mapped to the IDs of their holds
 * public Map<Integer, SeatHold> getSeatHolds();
 * 
 * // Removes all the seat holds of the given IDs
 * public void removeAllSeatHolds(List<Integer> seatHoldIds);
 * 
 * // Holds a list of Seats for the given customer email address. Creates IDs and updates the in-memory data model with
 * // the held seats
 * public SeatHold newSeatHold(String customerEmail, List<Seat> seatsToHold);
 * 
 * // Returns a seat hold for the given ID. Returns null if one wasn't found
 * public SeatHold getSeatHold(int id);
 * 
 * // Removes a seat hold from the database of seat holds
 * public void removeSeatHold(int id);
 * 
 * // Returns any expired seats into the pool of available seats.
 * public void reclaimExpiredHolds();
 * 
 * // Returns the given confirmation for a valid id, null otherwise
 * public ReservationConfirmation getReservationConfirmation(String confirmationNumber);
 * 
 * // Saves the given confirmation
 * public void saveReservationConfirmation(ReservationConfirmation confirmation);
 * 
 * // Returns all reservation confirmations as a list
 * public List<ReservationConfirmation> getReservationConfirmations();
 * </pre>
 * 
 */
public class SeatBookingEngine extends SeatBookingService {

	public SeatBookingEngine() {
		expireOldHolds();
	}

	private void expireOldHolds() {
		Runnable expirationTask = () -> {
			while (true) {
				dao.reclaimExpiredHolds();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.err.println("Issue when checking expired holds " + e.getMessage());
				}
			}
		};
		new Thread(expirationTask).start();
	}

	/**
	 * The number of seats that are neither held nor reserved
	 *
	 * @return the number of seats available in the venue
	 */
	@Override
	public int numSeatsAvailable() {
		// TODO: Leverage the underlying DAO to perform this operation
		return 0;
	}

	/**
	 * Find and hold the best available seats for a customer.
	 * <p>
	 * Note: Customers who book together prefer to sit near each other whenever possible. Seats are preferable nearest the stage.
	 * </p>
	 * 
	 * @param numSeats      the number of seats to find and hold
	 * @param customerEmail unique identifier for the customer
	 * @return a SeatHold object identifying the specific seats and related information
	 */
	@Override
	public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
		// TODO: Leverage the underlying DAO to perform this operation
		throw new RuntimeException("Not yet implemented");
	}

	/**
	 * Reserve unexpired seats held for a specific customer
	 * 
	 * Implementation note:
	 * 
	 * <pre>
	 * - If a 'hold' cannot be found for a given seatHoldId, return SeatBookingService.HOLD_NOT_FOUND_MESSAGE
	 * - If a 'hold' doesn't exist for the given customerEmail, return SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE
	 * </pre>
	 * 
	 *
	 * @param seatHoldId    the seat hold identifier
	 * @param customerEmail the email address of the customer to which the seat hold is assigned
	 * @return a reservation confirmation code
	 */
	@Override
	public String reserveSeats(int seatHoldId, String customerEmail) {
		// TODO: Leverage the underlying DAO to perform this operation
		return "";
	}
}
