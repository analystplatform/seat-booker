# seat-booker

Book seats at an event

## Prerequisites

This program requires the Java 11 JDK or later to be installed on the user's system.  [See here](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html) for instructions

## Check out the code

Use git to clone the code with the command:

    git clone https://bitbucket.org/analystplatform/seat-booker.git    

## Build and test the code

The project can be run by navigating to the directory where the code was checked-out, such as `~/git/seat-booker` and executing the gradle wrapper appropriate for your operating system.

On MacOSX and Linux, the command is `./gradlew`.  On windows, execute `gradlew.bat`.  The instructions in this README assume Linux or MacOSX, replace the command below as appropriate.

### Test the code

From the project's directory, execute:

    ./gradlew test

Test results will be printed to the console

### Build the code

From the project's directory, execute:

    ./gradlew build

And an executable jar will be built into `build/libs/`

## Configure in Eclipse

From the project's directory, execute:

    ./gradlew eclipse

In Eclipse, choose Import > "Existing Projects into Workspace"

Enter the root location of the checked-out project and click "Finish"

## Run the code

To run the code, execute a build using the steps above, and from the project root, invoke:

    java -jar build/libs/seat-booker.jar 

The program will present a list of interactive menu options in the console, similar to:

    Choose an option from the menu below:
      1.) Print available seat count
      2.) Hold a given number of seats
      3.) Print held seats
      4.) Reserve held seats
      5.) Print reservations
      
      9.) Exit

**Note:** To reserve seats, it may be helpful to choose option `3.)`  to print the seats which are held, along with their hold ID and email address.

Once any changes are made, re-build the application prior to running it in order for the changes to take effect.

# Instructions

Implement a simple seat booking service which facilitates the discovery, temporary hold, and final reservation of seats within a high-demand performance venue.

For example, note the example seating arrangement below.

    ----------[[ STAGE ]]----------
    ---------------------------------
    sssssssssssssssssssssssssssssssss
    sssssssssssssssssssssssssssssssss
    sssssssssssssssssssssssssssssssss
    sssssssssssssssssssssssssssssssss
    sssssssssssssssssssssssssssssssss
    sssssssssssssssssssssssssssssssss

Implement the `SeatBookingEngine` class to conform with the following instructions:

* Find the number of seats available within the venue
    + Available seats are seats that are neither held nor reserved.
* Find and hold the best available seats on behalf of a customer
    + Customers who book together prefer to sit near each other whenever possible.  Seats are preferable nearest the stage.  
    + Customers should be able to place a hold for any number of seats less than or equal to the number of available seats.  
* Reserve and commit a specific group of held seats for a customer
    + Each 'held' seat expires after 60 seconds.  Users should not be able to reserve expired seat holds.  Expired seats should be made available for booking.

Please add tests for submitted code.  The javadoc on SeatBookingEngine includes tips on utilizing the underlying data access object (DAO) to conduct seat holds, reservations, and expirations.
Submissions should not be concerned with synchronization and race conditions.  

Be prepared to present your solution to a team of engineers as if they were coworkers doing a peer review.  
